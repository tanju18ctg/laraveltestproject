

//-------counter-design--------------//

document.addEventListener("DOMContentLoader", () => {
    function counter(id, start, end, duration) {
        let obj = document.getElementById(id),
        current = start,
        range = end - start,
        increment = end > start ? 1 : -1,
        step = Math.obs(Math.floor(duration / range)),
        timer = setInterval( () => {
            current+= increment;
            obj.textContent = current;
            if(current == end)
            {
                clearInterval(timer);
            }

        }, step);

    }
    counter("count1", 0, 1287, 3000);
    counter("count2", 100, 12700, 2500);
    counter("count3", 200, 4287, 6000);
    counter("count4", 300, 2487, 6700);
});

