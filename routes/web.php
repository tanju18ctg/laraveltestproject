<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShortLinkController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\frontend\FrontendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.home');
// });

Auth::routes();


Route::get('/',[FrontendController::class, 'index'] );

Route::group(['prefix'=>'admin','middleware' =>['admin','auth'],'namespace'=>'admin'], function(){
    Route::get('dashboard', [AdminController::class, 'index'])->name('admin.dashboard');

});

Route::group(['prefix'=>'user','middleware' =>['user','auth'],'namespace'=>'user'], function()
{
    Route::get('dashboard', [UserController::class, 'index'])->name('user.dashboard');
    Route::get('update/profile',[UserController::class, 'updateData'])->name('update-profile');

});


// Shortend Link Routes 
Route::get('generate/shortlink', [ShortLinkController::class, 'index']);
Route::post('generate/shortlink', [ShortLinkController::class, 'store'])->name('short-link-post');
Route::get('{code}', [ShortLinkController::class, 'shortenLink'])->name('short-link');



