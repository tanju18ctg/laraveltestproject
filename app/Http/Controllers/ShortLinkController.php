<?php

namespace App\Http\Controllers;

use App\Models\ShortLink;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ShortLinkController extends Controller
{
    //short link index page
    public function index()
    {
        $shortLinks = ShortLink::latest()->get();
        return view('shortLink.index', compact('shortLinks'));
    }


    //short list store method

    public function store(Request $request){
        $request->validate([
            'link' => 'required|url'
        ]);

        ShortLink::insert([
            'link' => $request->link,
            'code' => Str::random(6)
        ]);

        return redirect()->to('generate/shortlink')->with('success', 'Shorten Link Generated Successfully!');


    }


    public function shortenLink($code){
       $code = ShortLink::where('code', $code)->first();
       return redirect($code->link);
    }


}
