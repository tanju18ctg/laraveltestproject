<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //index 
    public function index(){
        return view('user.home');
    }



        public function updateData(Request $request) 
        {
            $request->validate([
                'name' => 'required',
                'phone' => 'required',
                'email' => 'required'
            ], 
                [
                    'name.required' => 'please input your name',
                    'phone.required' => 'please input your phone',
                    'email.required' => 'please input your email',
                ]
        );
    
            User::findOrFail(Auth::id())->update([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
            ]);
    
            $notification = array(
                'message' => 'User Profile Updated',
                'alert-type' => 'success'
            );
            
            return Redirect()->back()->with($notification);
        }
    }

