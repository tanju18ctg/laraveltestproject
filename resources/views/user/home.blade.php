@extends('layouts.frontend-master')

@section('content')
    <div class="breadcrumb">
        <div class="container">
            <div class="breadcrumb-inner">
                <ul class="list-inline list-unstyled">
                    <li><a href="{{ url('/') }}" class="btn btn-primary">Home</a></li>
                    <li><a href="{{ route('login') }}" class="btn btn-primary">Login</a></li>
                  
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                   
                    
                </ul>
            </div><!-- /.breadcrumb-inner -->
        </div><!-- /.container -->
    </div><!-- /.breadcrumb -->

    <div class="body-content">
        <div class="container">
            <div class="sign-in-page">
                <div class="row">

                    <div class="col-sm-8 m-auto">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Hi: {{ Auth::user()->name }} <span> Update Profile</span> </h3>

                                <form action="{{ route('update-profile') }}">
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control"
                                            value="{{ Auth::user()->name }}">
                                        @error('name')
                                            {{ $message }}
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="phone" class="form-control"
                                            value="{{ Auth::user()->phone }}">
                                        @error('phone')
                                            {{ $message }}
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <input type="text" name="email" class="form-control"
                                            value="{{ Auth::user()->email }}">
                                        @error('email')
                                            {{ $message }}
                                        @enderror
                                    </div>
                                    <button class="btn btn-success btn-sm"> Update </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
