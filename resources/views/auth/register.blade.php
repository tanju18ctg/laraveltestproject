@extends('layouts.frontend-master')

@section('content')
    <div class="container">
        <div class="breadcrumb">
            <div class="container">
                <div class="breadcrumb-inner">
                    <ul class="list-inline list-unstyled">
                        <li><a href="home.html">Home</a></li>
                        <li class='active'>Login</li>
                    </ul>
                </div><!-- /.breadcrumb-inner -->
            </div><!-- /.container -->
        </div><!-- /.breadcrumb -->

        <div class="body-content">
            <div class="container">
                <div class="sign-in-page signup">
                    <div class="row">
                        <!-- create a new account -->
                        <div class="col-md-8 col-sm-8 create-new-account m-auto">
                            <h4 class="checkout-subtitle">Create a new account</h4>
                            <p class="text title-tag-line">Create your new account.</p>
                            <form class="register-form outer-top-xs" action="{{ route('register') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail1">Name <span>*</span></label>
                                    <input type="text"
                                        class="form-control unicase-form-control text-input @error('name') is-invalid @enderror"
                                        id="exampleInputEmail1" value="{{ old('name') }}" name="name">
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail2">Email Address <span>*</span></label>
                                    <input type="email" name="email"
                                        class="form-control unicase-form-control text-input @error('email') is-invalid @enderror"
                                        id="exampleInputEmail2" value="{{ old('email') }}">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail1">Phone Number <span>*</span></label>
                                    <input type="text" name="phone"
                                        class="form-control unicase-form-control text-input @error('phone') is-invalid @enderror"
                                        id="exampleInputEmail1">
                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail1">Password <span>*</span></label>
                                    <input type="password"
                                        class="form-control unicase-form-control text-input @error('password') is-invalid @enderror"
                                        id="exampleInputEmail1" name="password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail1">Confirm Password
                                        <span>*</span></label>
                                    <input type="password" class="form-control unicase-form-control text-input"
                                        id="exampleInputEmail1" name="password_confirmation">
                                </div>
                                <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Sign
                                    Up</button>
                            </form>

                        </div>
                        <!-- create a new account -->
                    </div><!-- /.row -->
                </div><!-- /.sigin-in-->
            </div><!-- /.container -->
        </div><!-- /.body-content -->
    </div>
@endsection
